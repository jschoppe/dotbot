import unittest

from Model.imageloader import ImageLoader
from Model.DetectionManager import CornerDetectionManager, AnswerDetectionManager, FormSetup
from Model.Parameters import RedScantron, DrBaneZipGrade
from Model.GeometryHandler import Image


class TestDetectionManager(unittest.TestCase):

    def setUp(self):
        self.params = DrBaneZipGrade()
        self.setup_image = Image(ImageLoader(self.params.image_path).load_image_number(0), self.params.layout)
        self.CP, self.AP = self.params()
        self.CornerDM = CornerDetectionManager(self.CP)
        self.AnswerDM = AnswerDetectionManager(self.AP, self.params.form_number)

    def test_corner_detection(self):
        self.CornerDM(self.setup_image)
        key = self.CornerDM.blobs()
        corners = self.CornerDM.corner_blobs(key)
        self.setup_image.show_key_points(corners)

    def test_square_image(self):
        self.CornerDM(self.setup_image)
        key = self.CornerDM.blobs()
        c = self.CornerDM.corner_points(key)
        image = self.setup_image.square_image(c[0], c[1], c[2])
        image.show_self()

    def test_mark_selection(self):
        self.CornerDM(self.setup_image)
        key = self.CornerDM.blobs()
        c = self.CornerDM.corner_points(key)
        image = self.setup_image.square_image(c[0], c[1], c[2])
        FS = FormSetup(image, self.AP)
        key = FS.find_marks()
        image.show_key_points(key)

    def test_Form_setup(self):
        self.CornerDM(self.setup_image)
        key = self.CornerDM.blobs()
        corners = self.CornerDM.corner_points(key)
        image = self.setup_image.square_image(*corners)
        FS = FormSetup(image, self.AP)
        key = FS.find_marks()
        marks = FS.remove_x_furthest_left_marks(0, key)
        sorted_marks = FS.find_section_bounding_marks(marks, [((2, 2), (2, 2)), ((2, 2), (2, 2))], self.params.form_number)
        FS.create_area_parameters(self.params.area_path,
                                  marks=sorted_marks,
                                  number_of_forms=2,
                                  answer_values=[((('A', 'B', 'C', 'D', 'E'), ('A', 'B', 'C', 'D', 'E')),
                                                  ([i for i in range(9, -1, -1)], ('A', 'B', 'C', 'D', 'E'))),
                                                 ((('A', 'B', 'C', 'D', 'E'), ('A', 'B', 'C', 'D', 'E')),
                                                  ([i for i in range(9, -1, -1)], ('A', 'B', 'C', 'D', 'E')))],
                                  answer_w_h=[(((26, 30), (26, 30)), ((26, 30), (26, 30))),
                                              (((26, 30), (26, 30)), ((26, 30), (26, 30)))],
                                  orientation=[(('horizontal', 'horizontal'), ('vertical', 'horizontal')),
                                               (('horizontal', 'horizontal'), ('vertical', 'horizontal'))],
                                  answer_range=[((list(range(8, 16)), list(range(1, 8))), (list(range(7)), list(range(16, 21)))),
                                                ((list(range(8, 16)), list(range(1, 8))), (list(range(7)), list(range(16, 21))))],
                                  range_name=[(('Answer Section 2', 'Answer Section 1'), ('Student ID', 'Answer Section 3')),
                                              (('Answer Section 2', 'Answer Section 1'), ('Student ID', 'Answer Section 3'))],
                                  image=image
        )
        self.params.load_areas()
        for i in range(self.params.form_number):
            for area in self.AP.areas[i]:
                image.show_programmed_boxes(area.locations, (area.answer_w, area.answer_h))


if __name__ == '__main__':
    unittest.main()
