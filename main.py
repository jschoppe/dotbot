from Presenter.MainPagePresenter import MainPagePresenter
from GUI.MainViewGUI import DotbotMainGUI
from GUI.ExamBrowserGUI import ExamBrowserGUI
from Model.grader import Grader
from tkinter import Tk


if __name__ == '__main__':
    app = Tk()
    app.title("Dotbot")
    MainPagePresenter(DotbotMainGUI(app), Grader(), ExamBrowserGUI)
    app.mainloop()
