# Version of ImageLoader that directly grabs images from PDF rather
#   than re-rendering the page as a Pixmap

import fitz
import cv2
import numpy as np
import imutils


class ImageLoader:

    """Extract an image or list of images from a file.

       loader = ImageLoader("some_file.pdf")
       loader.images will be the list of images from the file
    """

    def __init__(self, fname):
        self.fname = fname
        self.pdf = fitz.open(fname)
        self.image_locations = self._xrefs()
        self.len = len(self.image_locations)
        fitz.Tools().store_shrink(100)

    def _xrefs(self):
        image_xrefs = []
        for pnum in range(len(self.pdf)):
            imagelist = self.pdf.getPageImageList(pnum)
            image_xrefs.extend([im[0] for im in imagelist])
        return image_xrefs

    def __iter__(self):
        for i in range(len(self.image_locations)):
            yield self.load_image_number(i)

    def __getitem__(self, item):
        return self.load_image_number(item)

    def load_all_images(self):
        return [self.load_image_number(i) for i in
                range(len(self.image_locations))]

    def load_image_number(self, imnum):
        return self._pixmap_to_image(fitz.Pixmap(self.pdf,
                                                 self.image_locations[imnum]))

    def rotate_image_to_vertical(self, image):
        if image.shape[0] < image.shape[1]:
            return imutils.rotate_bound(image, 90)
        else:
            return image

    def close(self):
        self.pdf.close()

    def _pixmap_to_image(self, pix):
        im = np.frombuffer(pix.samples, np.uint8).reshape(pix.h, pix.w, pix.n)
        try:
            im = cv2.cvtColor(im, cv2.COLOR_RGB2BGR)
        except cv2.error:
            im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
        return im

    def load_single_image(self):
        return cv2.imread(self.fname)
