from Model.DataStructures import Point, Area
from math import sqrt, inf
import cv2
import pickle as pkl
import numpy as np


class CornerDetectionManager:

    def __init__(self, params):
        self._im = None
        self.params = params

    def __call__(self, image):
        self._im = image

    def blobs(self):
        blobdetector = cv2.SimpleBlobDetector_create(self.params)
        if self.params.marking_color == "red":
            red_image = self._im.red_image()
            thresh = red_image.threshed_image()
            return blobdetector.detect(thresh.image())
        elif self.params.marking_color == "black":
            black_image = self._im.black_image()
            return blobdetector.detect(black_image.image())

    def corner_points(self, points=(cv2.KeyPoint,)):
        upleft = self.closest_point((0, 0), points)
        upright = self.closest_point((self._im.width, 0), points)
        botleft = self.closest_point((0, self._im.height), points)
        return Point(*upleft.pt[:2]), Point(*upright.pt[:2]), Point(
            *botleft.pt[:2])

    def corner_blobs(self, points=(cv2.KeyPoint,)):
        upleft = self.closest_point((0, 0), points)
        upright = self.closest_point((self._im.width, 0), points)
        botleft = self.closest_point((0, self._im.height), points)
        return upleft, upright, botleft

    @staticmethod
    def _distance(p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    def closest_point(self, p1, points):
        dist = inf
        pt = points[0]
        for point in points:
            dis = self._distance(p1, point.pt)
            if dis < dist:
                dist = dis
                pt = point
        return pt


class AnswerDetectionManager:

    def __init__(self, params, form_number):
        self._im = None
        self.params = params
        self._form_number = form_number

    def __call__(self, image):
        self._im = image

    def locate_answer_marks(self):
        return self.find_marks()

    def find_marks(self):
        blobdetector = cv2.SimpleBlobDetector_create(self.params)
        if self.params.marking_color == "red":
            red_image = self._im.red_image()
            thresh = red_image.threshed_image()
            return blobdetector.detect(thresh.image())
        elif self.params.marking_color == "black":
            black_image = self._im.black_image()
            return blobdetector.detect(black_image.image())

    def check_locations(self):
        areas = self.params.areas[self._form_number-1]
        if self.params.marking_color == "red":
            image = self._im.red_image()
        elif self.params.marking_color == "black":
            image = self._im.black_image()
        inverted = image.inverted_image()
        answer_structure = {}
        for area in areas:
            answer_structure[area.name] = area.answer_data_structure()
            areas_with_marks = []
            for loc_index in range(len(area.locations)):
                image_section = inverted.image_section(area.locations[loc_index], area.answer_h, area.answer_w)
                if image_section.image_average_coverage_value() > self.params.min_coverage_value:
                    areas_with_marks.append(area.answer_value[loc_index])
            for number, value in areas_with_marks:
                if type(answer_structure[area.name]) is str:
                    answer_structure[area.name] += str(value)
                else:
                    answer_structure[area.name][number] += str(value)
        return answer_structure


class FormSetup:

    def __init__(self, image, params):
        self._im = image
        self.params = params

    def locate_answer_marks(self):
        return self.find_marks()

    def find_marks(self):
        blobdetector = cv2.SimpleBlobDetector_create(self.params)
        if self.params.marking_color == "red":
            ret,threshimage = cv2.threshold(self._im.red_image().image(),150,255,0)
            return blobdetector.detect(threshimage)
        elif self.params.marking_color == "black":
            return blobdetector.detect(self._im.black_image().image())

    def find_section_bounding_marks(self, marks, form_layout=(((2, ), (2, ), (2, 2)), ), number_of_forms=1):
        form = []
        marks_to_look_at = marks.copy()
        for i in range(number_of_forms):
            answer_section_bounding_marks = []
            for column in range(len(form_layout[i])):
                num_of_marks = sum(form_layout[i][column])
                marks_to_look_at = self.sort_marks_by_x(marks_to_look_at)
                answer_section_bounding_marks.append(marks_to_look_at[:num_of_marks])
                marks_to_look_at = marks_to_look_at[num_of_marks:]
                if len(form_layout[i][column]) > 1:
                    vertical_sections = []
                    marks_in_column = answer_section_bounding_marks[column]
                    column_marks_to_look_at = self.sort_marks_by_y(marks_in_column)
                    for row in range(len(form_layout[i][column])):
                        marks_in_row = form_layout[i][column][row]
                        vertical_sections.append(column_marks_to_look_at[:marks_in_row])
                        column_marks_to_look_at = column_marks_to_look_at[marks_in_row:]
                    answer_section_bounding_marks[column] = vertical_sections
                else:
                    answer_section_bounding_marks[column] = [answer_section_bounding_marks[column]]
            form.append(answer_section_bounding_marks)
        return form

    def remove_x_furthest_left_marks(self, X, marks):
        sortedmarks = sorted(marks.copy(), key=lambda x: x.pt[0])
        return sortedmarks[X:]

    def remove_x_furthest_top_marks(self, X, marks):
        sortedmarks = sorted(marks.copy(), key=lambda x: x.pt[1], reverse=True)
        return sortedmarks[X:]

    def remove_x_furthest_right_marks(self, X, marks):
        sortedmarks = sorted(marks.copy(), key=lambda x: x.pt[0], reverse=True)
        return sortedmarks[X:]

    def remove_x_furthest_bottom_marks(self, X, marks):
        sortedmarks = sorted(marks.copy(), key=lambda x: x.pt[1])
        return sortedmarks[X:]

    def create_area_parameters(self, filename, marks, number_of_forms, answer_values, orientation, answer_range, range_name, answer_w_h, image):
        forms = []
        for i in range(number_of_forms):
            areas = []
            for column in range(len(marks[i])):
                for row in range(len(marks[i][column])):
                    lowleft, upright = self.sort_marks_by_x(marks[i][column][row])
                    w, h = answer_w_h[i][column][row]
                    w = image.fixed_width(w)
                    h = image.fixed_height(h)
                    if orientation[i][column][row] == 'vertical':
                        areas.append(Area(image.fixed_coord(Point(*lowleft.pt)),
                                          image.fixed_coord(Point(*upright.pt)),
                                          answer_range[i][column][row],
                                          answer_values[i][column][row],
                                          orientation[i][column][row],
                                          range_name[i][column][row], (w, h)))
                    else:
                        areas.append(Area(image.fixed_coord(Point(*lowleft.pt)),
                                          image.fixed_coord(Point(*upright.pt)),
                                          answer_values[i][column][row],
                                          answer_range[i][column][row],
                                          orientation[i][column][row],
                                          range_name[i][column][row], (w, h)))
            forms.append(areas)
        with open(filename, 'wb') as file:
            pkl.dump(forms, file)

    def sort_marks_by_x(self, marks, reverse=False):
        return sorted(marks.copy(), key=lambda x: x.pt[0], reverse=reverse)

    def sort_marks_by_y(self, marks, reverse=False):
        return sorted(marks.copy(), key=lambda x: x.pt[1], reverse=not reverse)
