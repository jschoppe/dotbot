from Model.DetectionManager import CornerDetectionManager, AnswerDetectionManager


class AnswerSheet:

    def __init__(self, image, parameters, first_question=1, last_question=100):
        """Takes an image file location and analyzes the image"""
        self.Image = image
        self.answer_range = list(range(first_question, last_question+1))
        self.CP, self.AP = parameters()
        self.CornerDM = CornerDetectionManager(self.CP)
        self.AnswerDM = AnswerDetectionManager(self.AP, parameters.form_number)
        self.results = {}

    def answers(self):
        """returns the filled in answers"""
        answers = {}
        for key in self.results.keys():
            if 'Answer' in key:
                answers.update(self.results[key])
        answers_in_range = {}
        for key in answers:
            if key in self.answer_range:
                answers_in_range[key] = answers[key]
        return answers_in_range

    def id(self):
        """returns the student ID"""
        return self.results.get('Student ID', '')

    def exam_id(self):
        """returns the exam ID"""
        return self.results.get('Exam ID', '')

    def compile_result(self):
        self.CornerDM(self.Image)
        keys = self.CornerDM.blobs()
        corners = self.CornerDM.corner_points(keys)
        image = self.Image.square_image(*corners)
        self.AnswerDM(image)
        self.results = self.AnswerDM.check_locations()
