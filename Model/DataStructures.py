from typing import NamedTuple


class Point(NamedTuple):
    x: int
    y: int


class Answer(NamedTuple):
    question_number: int
    answer: str

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(tuple([self.question_number, self.answer]))


class Area:

    def __init__(self, lowleft, upright, cols, rows, orient, name, w_h):
        self.lowleft = lowleft
        self.upright = upright
        self.cols = cols
        self.rows = rows
        self.orient = orient
        self.name = name
        self.answer_w, self.answer_h = w_h
        self._set_locations()

    def _set_locations(self):
        dx = (self.upright.x - self.lowleft.x) / (len(self.cols)-1)
        dy = (self.upright.y - self.lowleft.y) / (len(self.rows)-1)
        locs = []
        number_value = []
        print(self.rows)
        for row in range(len(self.rows)):
            for col in range(len(self.cols)):
                if self.orient == 'vertical':
                    number_value.append((self.cols[col], self.rows[row]))
                else:
                    number_value.append((self.rows[row], self.cols[col]))
                locs.append(Point(self.lowleft.x+dx*col, self.upright.y-dy*row))
        self.locations = locs
        self.answer_value = number_value

    def answer_data_structure(self):
        if 'Answer Section' in self.name:
            structure = {}
            if self.orient == 'vertical':
                for number in self.cols:
                    structure[number] = ''
            else:
                for number in self.rows:
                    structure[number] = ''
        else:
            structure = ''
        return structure
