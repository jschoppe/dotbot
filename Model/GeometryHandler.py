import imutils
import cv2
import numpy as np
from math import sqrt
from Model.DataStructures import Point, Area


class Image:
    """A class designed to encapsulate the useful method for the manipulation of an image"""

    def __init__(self, image, layout="portrait"):
        self._im = image
        self._layout = layout
        self._update()
        if self.aspect < 1 and layout == "portrait":
            self.rotate_image_by_90()
        elif self.aspect > 1 and layout == "landscape":
            self.rotate_image_by_90()
        self._set_coordinates()

    def _set_dimensions(self):
        self.shape = self._im.shape
        self.width = self._im.shape[1]
        self.height = self._im.shape[0]
        self.aspect = self.height / self.width

    def rotate_image_counter_clockwise(self, deg):
        self._im = imutils.rotate(self._im, deg)
        self._update()

    def rotate_image_by_90(self, numtime=1):
        self._im = imutils.rotate_bound(self._im, 90 * numtime)
        self._update()

    def threshed_image(self):
        return Image(cv2.threshold(self._im, 150, 255, 0)[1], self._layout)

    def inverted_image(self):
        return Image(cv2.bitwise_not(self._im), self._layout)

    def gray_image(self):
        return Image(cv2.cvtColor(self._im, cv2.COLOR_BGR2GRAY), self._layout)

    def red_image(self):
        b, g, r = cv2.split(self._im)
        blurred = cv2.GaussianBlur(r, (11, 11), 0)
        return Image(blurred, self._layout)

    def black_image(self):
        im = self.gray_image().image()
        blurred = cv2.GaussianBlur(im, (11, 11), 0)
        return Image(blurred, self._layout)

    def image(self):
        return self._im

    def image_section(self, center, h, w):
        x, y = center
        x_low, y_low = self.image_coord(Point(x-w/2, y-h/2))
        x_high, y_high = self.image_coord(Point(x+w/2, y+h/2))
        return Image(self._im[round(y_low):round(y_high), round(x_low):round(x_high)], self._layout)

    def image_value(self):
        return np.sum(self._im)

    def image_average_coverage_value(self):
        return self.image_value()/(self.width*self.height)

    def _set_coordinates(self):
        self._coord = Transform(self.width, self.height)

    def square_image(self, upleft, upright, botleft):
        dis = 0
        src = np.float32([upleft, upright, botleft])
        dst = np.float32([Point(dis, dis),
                          Point(self._distance(upleft, upright) + dis, dis),
                          Point(dis, self._distance(upleft, botleft) + dis),
                          ])
        M = cv2.getAffineTransform(src, dst)
        return Image(cv2.warpAffine(self._im, M, (self.width, self.height)), self._layout)

    def image_coord(self, pt):
        return self._coord.screen(pt)

    def fixed_coord(self, pt):
        return self._coord.world(pt)

    def fixed_width(self, dis):
        return self._coord.width_world(dis)

    def fixed_height(self, dis):
        return self._coord.height_world(dis)

    def _update(self):
        self._set_dimensions()

    def _resize(self, im):
        return Image(imutils.resize(im, width=800), self._layout)

    @staticmethod
    def _distance(p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        return int(sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))

    def resize(self):
        return self._resize(self._im)

    def copy(self):
        return Image(self._im.copy(), self._layout)

    def show_programmed_boxes(self, loc, w_h):
        im = self._im.copy()
        w, h = w_h
        w = self._coord.width_screen(w)
        h = self._coord.height_screen(h)
        for i in range(len(loc)):
            x, y = self.image_coord(loc[i])
            cv2.rectangle(im, (int(x-w/2), int(y-h/2)), (int(x+w/2), int(y+h/2)), (255, 0, 0), 2)
        self.show_image("Rectangles", im)

    def show_key_points(self, keypoints):
        im = cv2.drawKeypoints(self._im, keypoints, np.array([]), (0, 255, 0),
                               cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        self.show_image("Blobs", im)

    def show_self(self, title="Image", resize=True):
        self.show_image(title, self.image(), resize)

    def show_image(self, title, image, resize=True):
        isinstance(title, str)
        isinstance(image, np.ndarray)
        if resize:
            cv2.imshow(title, self._resize(image).image())
        else:
            cv2.imshow(title, image)
        cv2.waitKey(0)


class Transform:
    """Internal class for 2-D coordinate transformations"""

    def __init__(self, w, h):
        # w, h are width and height of window
        # xspan, yspan are the new range of the coords starting from the top left of the image
        # if they are changed from the default 1000 you will have to re-setup each of the forms
        xspan = 1000
        yspan = 1000
        self.xscale = xspan / float(w - 1)
        self.yscale = yspan / float(h - 1)

    def screen(self, pt):
        # Returns x,y in screen (actually window) coordinates
        xs = pt.x / self.xscale
        ys = pt.y / self.yscale
        return Point(int(xs), int(ys))

    def world(self, pt):
        # Returns xs,ys in world coordinates
        x = pt.x * self.xscale
        y = pt.y * self.yscale
        return Point(int(x), int(y))

    def width_screen(self, dis):
        # Returns an x distance in pixel length from an x distance given in standard coords
        return dis / self.xscale

    def height_screen(self, dis):
        # Returns a y distance in pixel length from a y distance given in standard coords
        return dis / self.yscale

    def width_world(self, dis):
        # Returns an x distance in standard coord length from an x distance given in pixels
        return dis * self.xscale

    def height_world(self, dis):
        # Returns a y distance in standard coord length from a y distance given in pixels
        return dis * self.yscale
