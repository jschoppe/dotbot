# grader.py
# dotbot iteration 2 super model

from Model.AnswerSheet import AnswerSheet
from Model.imageloader import ImageLoader
from Model.scoring import score_exam, save_csv
from Model.GeometryHandler import Image
from Model.Parameters import *
import threading
import os.path as path
import os


class Grader:

    def __init__(self):
        self._image_num = 1
        self.key_im = None
        self.key = {}
        self._id_name = {}
        self.question_discriminant = {}
        self.ids = []
        self.exam_answers = []
        self._name_score = []
        self._id_score = []
        self._loader = None
        self._exams_file = ""
        self.first_question = 1
        self.last_question = 100
        self._loader = None
        self.lock = threading.Lock()
        self.id_score_change = False

    def set_file(self, fname):
        self._image_num = 1
        self._exams_file = fname
        self._set_loader()
        self._set_initial_data_structures()
        self._set_key()
        self._set_id_name_relation()

    def _set_loader(self):
        if self._loader is not None:
            self._loader.close()
        self._loader = ImageLoader(self._exams_file)

    def set_key_answers(self, start, end, formnumber, formname):
        self._set_form_parameters(eval(formname)(), formnumber)
        self.sheet_parameters.load_areas()
        self.first_question = start
        self.last_question = end
        self.key_im = Image(self.key_im.image(), self.sheet_parameters.layout)
        sheet = AnswerSheet(self.key_im, self.sheet_parameters, first_question=start, last_question=end)
        sheet.compile_result()
        self.key = sheet.answers()

    def _set_key(self):
        self.key_im = Image(self._loader.load_image_number(0))

    def _set_initial_data_structures(self):
        self.exam_answers = [None] * self._loader.len
        self.ids = [None] * self._loader.len
        self._id_score = [None] * self._loader.len
        self._name_score = [None] * self._loader.len
        self._score_answers = [None] * self._loader.len
        self._name_answers = [None] * self._loader.len
        self.examset_analysis = [('#', 'Answer', '# Correct', '% Correct', 'Discrim. Factor', 'Alt. Answers')]

    def _set_form_parameters(self, sheet, form_number):
        self.sheet_parameters = sheet
        self.sheet_parameters.set_form_number(form_number)

    def _set_id_name_relation(self):
        self._id_name = {}
        directory, _ = path.split(self._exams_file)
        file_path = path.join(directory, 'StudentRoster.csv')
        with open(file_path) as file:
            file_data = file.readlines()
        for line in file_data:
            line = line.strip("\n")
            line = line.split(',')
            ids = line[0].lstrip('0')
            name = ' '.join(line[1:])
            self._id_name[ids] = name

    def autoscore_examset(self):
        image_num = 0
        while image_num < self._loader.len - 1:
            image_num += 1
            self.autoscore_exam(image_num)
            yield self._name_score[image_num]

    def autoscore_current_exam(self):
        self.autoscore_exam(self._image_num)

    def autoscore_exam(self, number):
        if self.exam_answers[number] is None:
            if not self.lock.locked():
                self.id_score_change = True
                self.lock.acquire()
                self._set_exam_answer_and_id(number)
                self._compile_id_with_score(number)
                self.lock.release()

    def _compile_id_with_score(self, num):
        score = score_exam(self.key, self.exam_answers[num])
        self._score_answers[num] = (score, self.exam_answers[num])
        self._id_score[num] = (self.ids[num],
                               str(score))
        self._name_score[num] = (self._id_name.get(self.ids[num], self.ids[num]),
                                 str(score))
        self._name_answers[num] = (self._id_name.get(self.ids[num], self.ids[num]),
                                   self.exam_answers[num])

    def current_image(self):
        self.autoscore_current_exam()
        image = Image(self._loader.load_image_number(self._image_num), self.sheet_parameters.layout)
        return (image,
                self._image_num,
                self._name_score[self._image_num])

    def image(self, image_num):
        return Image(self._loader.load_image_number(image_num), self.sheet_parameters.layout)

    def _set_exam_answer_and_id(self, num):
        sheet = AnswerSheet(Image(self._loader.load_image_number(num), self.sheet_parameters.layout),
                            self.sheet_parameters,
                            first_question=self.first_question,
                            last_question=self.last_question)
        sheet.compile_result()
        self.exam_answers[num] = sheet.answers()
        self.ids[num] = sheet.id().lstrip('0')

    def key_image(self):
        return self.key_im

    def next_image(self):
        if self._image_num < self._loader.len - 1:
            self._image_num += 1

    def prev_image(self):
        if self._image_num > 1:
            self._image_num -= 1

    def get_examset_fname(self):
        return self._exams_file

    def get_exam_answers(self):
        return self.exam_answers

    def save_to_file(self, title):

        save_csv(self._exams_file, title, self.last_question-self.first_question+1,
                 self._id_score)

    def save_data_to_file(self):
        directory, file = path.split(self._exams_file)
        filename, _ = path.splitext(file)
        dest_dir = path.join(directory, filename)
        if not path.exists(dest_dir):
            os.mkdir(dest_dir)
        student_dir = path.join(dest_dir, 'StudentData')
        if not path.exists(student_dir):
            os.mkdir(student_dir)
        self.item_analysis()
        item_analysis_file_path = path.join(dest_dir, "AnswerAnalysisForm{}.csv".format(self.sheet_parameters.form_number))
        if path.exists(item_analysis_file_path):
            os.remove(item_analysis_file_path)
        with open(item_analysis_file_path, 'a') as file:
            for line in self.examset_analysis:
                file.write(','.join(line))
                file.write('\n')
        for test, ids in zip(self._name_answers, self._id_score):
            id, score = ids
            name, answers = test
            file = path.join(student_dir, '{}_{}_Form{}.txt'.format(id, name.replace(' ', '_'), str(self.sheet_parameters.form_number)))
            compiled = path.join(item_analysis_file_path, 'CompiledScoresForm{}.csv'.format(str(self.sheet_parameters.form_number)))
            if path.exists(file):
                os.remove(file)
            if path.exists(compiled):
                os.remove(compiled)
            with open(file, 'a') as openfile:
                openfile.write("{}: {}\n".format(name, score))
                openfile.write("# | Correct Ans. | Student Ans.\n")
                for key in self.key.keys():
                    openfile.write("{} | {}             {}\n".format(key, self.key[key], answers[key]))
            with open(compiled, 'a') as file:
                file.write("{}, {}\n".format(score, name))

    def get_image_number(self):
        return self._image_num

    def get_num_of_questions(self):
        return self.last_question-self.first_question+1

    def check_change(self):
        return self.id_score_change

    def acknowledge_change(self):
        self.id_score_change = False

    def sampleforms(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        form_dir = path.join(dir_path, 'UsableForms')
        onlyfiles = [Image(ImageLoader(path.join(form_dir, f)).load_image_number(0)) for f in os.listdir(form_dir) if path.isfile(path.join(form_dir, f))]
        return onlyfiles

    def samplefilenames(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        form_dir = path.join(dir_path, 'UsableForms')
        onlyfiles = [path.splitext(f)[0] for f in os.listdir(form_dir) if path.isfile(path.join(form_dir, f))]
        return onlyfiles

    def generate_discriminant(self):
        number_of_exams = len(self.exam_answers)-1
        number_of_exams_in_20_percent = int(number_of_exams*.20) if number_of_exams*.20 > 0 else 1
        sorted_exam_scores = sorted(self._score_answers, key=lambda x: x[0])
        top_20_percent = [score_answer[1] for score_answer in sorted_exam_scores[-number_of_exams_in_20_percent:]]
        bottom_20_percent = [score_answer[1] for score_answer in sorted_exam_scores[:number_of_exams_in_20_percent]]
        self.question_discriminant = {}
        for key in self.key.keys():
            top_correct = 0
            bottom_correct = 0
            for exam in top_20_percent:
                if exam[key] == self.key[key]:
                    top_correct += 1
            for exam in bottom_20_percent:
                if exam[key] == self.key[key]:
                    bottom_correct += 1
            self.question_discriminant[key] = round((top_correct-bottom_correct)/number_of_exams_in_20_percent, 3)
        return self.question_discriminant

    def item_analysis(self):
        """('#', 'Answer', '# Correct', '% Correct', 'Discrim. Factor', 'Alt. Answers')"""
        if self._score_answers[1:] != [None] * (self._loader.len-1):
            self.generate_discriminant()
            question_number = self.key.keys()
            question_number = sorted(question_number)
            for number in question_number:
                others = {}
                number_correct = 0
                for exam in self.exam_answers[1:]:
                    if exam[number] == self.key[number]:
                        number_correct += 1
                    else:
                        if exam[number] == '':
                            count = others.get('_', 0)
                            others['_'] = count + 1
                        else:
                            count = others.get(exam[number], 0)
                            others[exam[number]] = count + 1

                for k in others.keys():
                    others[k] = str(round(others[k]/len(self.exam_answers[1:])*100))+'%'
                self.examset_analysis.append((str(number),
                                              str(self.key[number]),
                                              str(number_correct),
                                              str(round(number_correct/len(self.exam_answers[1:])*100, 1))+'%',
                                              str(self.question_discriminant[number]),
                                              str(others).strip('{}').replace('\'', '')))

    def image_num(self):
        return self._image_num
