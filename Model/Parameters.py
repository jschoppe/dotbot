import cv2
import pickle as pkl
import os


class CornerParameters(cv2.SimpleBlobDetector_Params):
    def __init__(self):
        super().__init__()
        # instance variables needed for form recognition that are not built-in to the blob detector
        self.marking_color = None


class AnswerParameters(cv2.SimpleBlobDetector_Params):

    def __init__(self):
        super().__init__()
        # instance variables needed for form recognition that are not built-in to the blob detector
        self.marking_color = None
        self.areas = None
        self.min_coverage_value = None


class FormParameters:

    def __init__(self):
        self.layout = "portrait"
        self.area_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'Areas')
        self.image_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'UsableForms')
        self.area_path = None
        self.image_path = None
        self.form_number = 1
        self.CornerParameters = CornerParameters()
        self._set_corner_parameters()
        self.AnswerParameters = AnswerParameters()
        self._set_answer_parameters()
        self._set_file_and_image_path()

    def __call__(self, *args, **kwargs):
        return self.CornerParameters, self.AnswerParameters

    def set_form_number(self, form_number):
        self.form_number = form_number

    def _set_answer_parameters(self):
        pass

    def _set_corner_parameters(self):
        pass

    def _set_file_and_image_path(self):
        pass


class RedScantron(FormParameters):

    def __call__(self, *args, **kwargs):
        return self.CornerParameters, self.AnswerParameters

    def set_form_number(self, form_number):
        self.form_number = form_number

    def _set_answer_parameters(self):
        # built-in blob detection parameters
        self.AnswerParameters.filterByArea = True
        self.AnswerParameters.minArea = 150
        self.AnswerParameters.maxArea = 5000
        self.AnswerParameters.filterByInertia = False
        self.AnswerParameters.filterByConvexity = False
        self.AnswerParameters.filterByCircularity = False
        self.AnswerParameters.filterByColor = False

        # additional parameters needed for detecting answer marks
        self.AnswerParameters.marking_color = "red"
        self.AnswerParameters.min_coverage_value = 35
        # loading in template areas from file

    def _set_corner_parameters(self):
        # built-in blob detection parameters
        self.CornerParameters.filterByArea = True
        self.CornerParameters.minArea = 200
        self.CornerParameters.maxArea = 5000
        self.CornerParameters.filterByInertia = False
        self.CornerParameters.filterByConvexity = False
        self.CornerParameters.filterByCircularity = False
        self.CornerParameters.filterByColor = False

        # additional parameters needed for detecting answer marks
        self.CornerParameters.marking_color = "red"

    def _set_file_and_image_path(self):
        self.area_path = os.path.join(self.area_loc, 'RedScantron.areas')
        self.image_path = os.path.join(self.image_loc, 'RedScantron.pdf')

    def load_areas(self):
        with open(self.area_path, 'rb') as out_file:
            self.AnswerParameters.areas = pkl.load(out_file)


class DrBaneZipGrade(FormParameters):

    def __init__(self):
        super().__init__()
        self.layout = "landscape"
        self.form_number = 2

    def __call__(self, *args, **kwargs):
        return self.CornerParameters, self.AnswerParameters

    def set_form_number(self, form_number):
        self.form_number = form_number

    def _set_answer_parameters(self):
        # built-in blob detection parameters
        self.AnswerParameters.filterByArea = True
        self.AnswerParameters.minArea = 500
        self.AnswerParameters.maxArea = 600
        self.AnswerParameters.filterByInertia = False
        self.AnswerParameters.filterByConvexity = False
        self.AnswerParameters.filterByCircularity = True
        self.AnswerParameters.minCircularity = .9
        self.AnswerParameters.filterByColor = False

        # additional parameters needed for detecting answer marks
        self.AnswerParameters.marking_color = "black"
        self.AnswerParameters.min_coverage_value = 35
        # loading in template areas from file

    def _set_corner_parameters(self):
        # built-in blob detection parameters
        self.CornerParameters.filterByArea = True
        self.CornerParameters.minArea = 200
        self.CornerParameters.maxArea = 5000
        self.CornerParameters.filterByInertia = False
        self.CornerParameters.filterByConvexity = False
        self.CornerParameters.filterByCircularity = False
        self.CornerParameters.filterByColor = False

        # additional parameters needed for detecting answer marks
        self.CornerParameters.marking_color = "black"

    def _set_file_and_image_path(self):
        self.area_path = os.path.join(self.area_loc, 'DrBaneZipGrade.areas')
        self.image_path = os.path.join(self.image_loc, 'DrBaneZipGrade.pdf')

    def load_areas(self):
        with open(self.area_path, 'rb') as out_file:
            self.AnswerParameters.areas = pkl.load(out_file)
