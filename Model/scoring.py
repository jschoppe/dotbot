# scoring.py
import os


def score_exam(key, exam):
    correct = 0
    for k in key.keys():
        if exam[k] == key[k]:
            correct += 1
    return correct


def save_csv(fname, title, possible_points, id_scores):
    file = os.path.splitext(fname)[0]
    with open(file+"-scores.csv", "w") as f:
        print(title, file=f)
        print(possible_points, file=f)
        for id_score in id_scores[1:]:
            if id_score is not None:
                try:
                    id_score = (id_score[0], id_score[1].split('/')[0],
                                str(eval(id_score[1])*100))
                except ZeroDivisionError:
                    id_score = id_score[0], "0"
                print(','.join(id_score), file=f)


def save_data(class_roster, test_name, test_data):
    pass
