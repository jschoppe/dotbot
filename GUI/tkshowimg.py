# demo display of cv image using tkinter (via PIL)
import cv2
import tkinter as tk
from PIL import Image, ImageTk


def convert_image_to_tk(img):
    pil_image = Image.fromarray(img[..., [2, 1, 0]])
    tk_image = ImageTk.PhotoImage(image=pil_image)
    return tk_image


def size_to_height(img, height):
    width = int(img.shape[1]/img.shape[0] * height)
    dim = (width, height)
    return cv2.resize(img, dim)


def size_to_width(img, width):
    height = int(img.shape[0]/img.shape[1] * width)
    dim = (width, height)
    return cv2.resize(img, dim)


def test():
    root = tk.Tk()
    cvimg = cv2.imread('key.jpg')
    cvimg = size_to_height(cvimg, 600)
    # !! NOTE: must save tk image to variable to avoid premature gc
    tkimg = convert_image_to_tk(cvimg)
    tk.Label(root, image=tkimg).grid()
    root.mainloop()


if __name__ == "__main__":
    test()
