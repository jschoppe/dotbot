from tkinter import ttk
from tkinter import Menu, Toplevel, IntVar, NS, DISABLED, NORMAL, NSEW
from tkinter import NW, Canvas, filedialog as fd, StringVar
from GUI.tkshowimg import convert_image_to_tk, size_to_width
import threading


class DotbotMainGUI:
    instructions = """
    Welcome to Dotbot 1.0.
    This is a Scantron specific grading tool designed by CS270 Group 1.
    Dotbot analyzes and grades the examset provided, and allows for browsing and
    auto-scoring of the examset alongside added analytical features.
    Click on "Open" button in the File menu to begin, and select a PDF containing
    Scantron documents. Then enter the number of questions the exam is to be 
    graded out of.
    """

    instructions2 = """
    Now that there is a document loaded the key answers should be displayed on
    the far left of the screen. To analyze the examset select analyze all from 
    the analyze menu. To view the exams, key or both select how you would like 
    to view them from the view menu. If you have not already analyzed the exams
    by browsing through the exams they with automatically analyze. The column to
    the right will display buttons with the id and score of analyzed exams when
    clicked will display the selected answers next to the appropriate key answer.
    The column to the left displays the selected answers from the selected exam/
    currently displayed exam. The view selected exam button when pressed will 
    show the current exam whose answers are displayed above it. To save the exam
    information to a csv select save from the file menu and enter a title for 
    the information to be displayed as the top line of the csv file.
    """

    def __init__(self, master=None):
        self.master = master
        self.screen_height = int(self.master.winfo_screenheight() * 7 / 8)
        self.master.geometry("900x600")
        self.browse_window = None

    def set_event_handler(self, event_handler):
        self.event_handler = event_handler

    def _create_main_gui(self):
        self.master.title("Dotbot Main Page")

    def _create_menu(self):
        self.dotbot_menu = Menu(self.master)
        self.master.config(menu=self.dotbot_menu)

    def _create_File_menu(self):
        file_menu = Menu(self.dotbot_menu)
        self.dotbot_menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Save Exam scores",
                              command=self.event_handler.save_to_file,
                              state=DISABLED)
        file_menu.add_command(label="Open",
                              command=self.event_handler.load_file)

    def _create_View_menu(self):
        view_menu = Menu(self.dotbot_menu)
        self.dotbot_menu.add_cascade(label="View", menu=view_menu)
        view_menu.add_command(label="View Exam set",
                              command=self.event_handler.display_examset,
                              state=DISABLED)
        view_menu.add_command(label="View Key",
                              command=self.event_handler.display_key,
                              state=DISABLED)
        view_menu.add_command(label="View Key and Exam set",
                              command=self.event_handler.display_key_examset,
                              state=DISABLED)

    def enable_all_menus(self):
        for child in self.dotbot_menu.winfo_children():
            for i in range(10):
                child.entryconfig(i, state=NORMAL)

    def insert_button(self, label, command):
        self.ButtonList.insert_button(label, command)

    def get_browse_window(self):
        if (self.browse_window is None or
                self.browse_window.winfo_exists() == 0):
            self.browse_window = Toplevel()
            self.browse_window.protocol("WM_DELETE_WINDOW", lambda:
            self.event_handler.handle_sub_window_closes(
                self.browse_window))
        return self.browse_window

    def _create_analyze_menu(self):
        analyze_menu = Menu(self.dotbot_menu)
        self.dotbot_menu.add_cascade(label="Analyze", menu=analyze_menu)
        analyze_menu.add_command(label="Analyze Exam set",
                                 command=self.event_handler.
                                 display_analyzing_progress_bar,
                                 state=DISABLED)

    def analyze_all(self, command, arg):
        def run(win):
            bar = ttk.Progressbar(win, orient="horizontal", length=200,
                                  mode="indeterminate")
            ttk.Label(win, text="Please Wait").grid()
            bar.grid()
            bar.start()
            win.mainloop()

        def analyze(win):
            command(arg)
            win.destroy()

        win = Toplevel()
        t1 = threading.Thread(target=analyze, args=(win,))
        t1.start()
        run(win)
        t1.join()

    def _create_Stats_menu(self):
        stats_menu = Menu(self.dotbot_menu)
        self.dotbot_menu.add_cascade(label="Stats", menu=stats_menu)
        stats_menu.add_command(label="Display Answer Analysis",
                               command=self.event_handler.display_exam_analysis,
                               state=DISABLED)

    def file_dialog(self):
        return fd.askopenfilename(filetypes=[("PDF files", "*.pdf")])

    def question_number_dialog(self, images, filenames):
        def maingui(*args):
            self.event_handler.analyze_key(first_question.get(), last_question.get(), form_number.get(), args[0])
            win.destroy()

        win = Toplevel()
        win.geometry("400x900")
        first_question = IntVar()
        last_question = IntVar()
        form_number = IntVar()
        ttk.Label(win, text="First Question:").grid(row=0, column=0)
        ttk.Label(win, text="Last Question:").grid(row=0, column=1)
        ttk.Label(win, text="Form Number").grid(row=0, column=2)
        ttk.Entry(win, textvariable=first_question).grid(row=1, column=0)
        ttk.Entry(win, textvariable=form_number).grid(row=1, column=2)
        entry = ttk.Entry(win, textvariable=last_question)
        first_question.set(1)
        last_question.set(100)
        form_number.set(1)
        scrollhandle = ScrollEventHandler()
        frame = VerticalScrolledFrame(win)
        scrollhandle.add_listener(frame)
        frame.grid(row=2, column=0, columnspan=3, sticky=NS)
        i = 0
        for image in images:
            images[i] = size_to_width(image.image(), 340)
            i += 1
        args = []
        for filename in filenames:
            args.append(filename)
        frame.insert_buttons_with_images(images, maingui, args)
        win.bind_all("<MouseWheel>", scrollhandle.scroll)
        entry.grid(row=1, column=1)
        entry.focus_set()
        win.rowconfigure(2, weight=10)
        win.bind("<Return>", maingui)

    def question_value(self, indexes):

        win = Toplevel()


    def change_instructions(self):
        self.instruction_label.configure(text=self.instructions2)

    def clear_window(self):
        for wid in self.master.winfo_children():
            wid.destroy()

    def exam_button(self):
        return self._exam_button

    def setup(self):
        self.clear_window()
        self._create_main_gui()
        self._create_menu()
        self._create_File_menu()
        self._create_View_menu()
        self._create_analyze_menu()
        self._create_Stats_menu()
        self._create_open_button()
        key_label = ttk.Label(self.master, text="Key Answers")
        exam_label = ttk.Label(self.master, text="Exam Answers")
        graded_label = ttk.Label(self.master, text="Graded Exams")
        key_label.grid(column=0, row=0, sticky="w", padx=20, pady=10)
        exam_label.grid(column=0, row=0, sticky="e", padx=17)
        graded_label.grid(column=2, row=0)
        scrollhandle = ScrollEventHandler()
        self.answerlist = VerticalScrolledFrame(self.master)
        scrollhandle.add_listener(self.answerlist)
        self.answerlist.grid(column=0, row=1, sticky=NS)
        self.instruction_label = ttk.Label(self.master, text=self.instructions)
        self.instruction_label.grid(column=1, row=1)
        self.ButtonList = VerticalScrolledFrame(self.master)
        self.ButtonList.setup_button_list()
        scrollhandle.add_listener(self.ButtonList)
        self.ButtonList.grid(column=2, row=1, sticky=NS)
        stats = ttk.Frame(self.master)
        stats.grid(column=3, row=1)
        self.average = ttk.Label(stats)
        self.average.grid()
        self.min = ttk.Label(stats)
        self.min.grid()
        self.max = ttk.Label(stats)
        self.max.grid()
        self.master.rowconfigure(1, weight=10)
        self.answerlist.setup_label_list(15)
        self.master.bind_all("<MouseWheel>", scrollhandle.scroll)

    def get_answerlist(self):
        return self.answerlist

    def insert_key_answers(self, answers):
        for index in answers.keys():
            self.key[index].configure(text=str(index) + ": " + answers[index])

    def insert_label_pairs(self, start, finish, width=0):
        self.answerlist.clear_window()
        self.key, self.exam = self.answerlist.insert_label_pairs(start, finish, width)

    def show_exam_answer(self, answers, show_command):
        self._exam_button.configure(command=show_command)
        for key in answers.keys():
            self.exam[key].configure(text=answers[key])

    def insert_ids_score_buttons(self, id_scores):
        self.ButtonList.clear_window()
        for i, id_score in enumerate(id_scores):
            self.insert_id_score_button(id_score, i+1)

    def insert_id_score_button(self, id_score, image_num):
        id, score = id_score
        label = "ID: " + id + "\nScore: " + score
        self.insert_button(
            label, self.event_handler.display_selected_exam(image_num))

    def _create_open_button(self):
        self._exam_button = ttk.Button(self.master, text="Open Selected Exam")
        self._exam_button.grid(column=0, row=2, pady=15)

    def display_exam_analysis(self, grid):
        win = Toplevel()
        win.geometry("500x600")
        scrollhandle = ScrollEventHandler()
        frame = VerticalScrolledFrame(win)
        scrollhandle.add_listener(frame)
        frame.grid(row=0, sticky=NS)
        win.rowconfigure(0, weight=10)
        widths = [2, 8, 9, 10, 16, 26]
        frame.insert_label_grid(grid, widths)
        win.bind_all("<MouseWheel>", scrollhandle.scroll)

    def close_browse_window(self):
        self.get_browse_window().destroy()


# https://gist.github.com/EugeneBakin/76c8f9bcec5b390e45df


class VerticalScrolledFrame(ttk.Frame):

    def __init__(self, parent, *args, **options):

        ttk.Frame.__init__(self, parent)
        self.__createWidgets()

    def __createWidgets(self):
        '''Create widgets of the scroll frame.'''
        self.vscrollbar = ttk.Scrollbar(self, orient='vertical')
        self.canvas = canvas = Canvas(self,
                                      bd=1,
                                      highlightthickness=1,
                                      yscrollcommand=self.vscrollbar.set)
        self.vscrollbar.pack(side='right', fill='y', expand='false')
        canvas.pack(side='left', fill='both', expand='true', padx=(20, 0))
        self.vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            canvas.config(background='white')
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind('<Configure>', _configure_canvas)

    def insert_label_entry_pairs(self, indexes):
        entries = {}
        row = 1
        ttk.Label(self.master, text='Question #:').grid(row=0, column=0)
        ttk.Label(self.master, text='Value:').grid(row=0, column=1)
        for key in indexes:
            entries[key] = IntVar()
            ttk.Label(self.master, text=str(key)).grid(row=row, column=0)
            ttk.Entry(self.master, textvariable=entries[key])
            entries[key].set(1)

    def insert_label_grid(self, grid, column_sizes):
        for row in range(len(grid)):
            for column in range(len(grid[row])):
                ttk.Label(self.interior, text=grid[row][column], width=column_sizes[column]).grid(row=row, column=column)

    def insert_button(self, label, command):
        ttk.Button(self.interior, text=label, command=command,
                   width=15).grid(column=0)

    def insert_buttons_with_images(self, images, command, args):
        i = 0
        for image, arg in zip(images, args):
            def call(argument):
                a = argument
                return lambda: command(a)
            wid = ttk.Button(self.interior, command=call(arg))
            image = convert_image_to_tk(image)
            wid.image = image
            wid.configure(image=image, width=395)
            wid.grid(row=i)
            i += 1

    def insert_label_pairs(self, first, last, width=0):
        row0 = {}
        row1 = {}
        row_num = 0
        for row in range(first, last + 1):
            row0[row] = ttk.Label(self.interior, width=width)
            row1[row] = ttk.Label(self.interior, width=width)
            row0[row].grid(column=0, row=row_num)
            row1[row].grid(column=1, row=row_num)
            row_num += 1
        return row0, row1

    def setup_button_list(self):
        ttk.Label(self.interior, width=15).grid(column=0)

    def setup_label_list(self, width):
        ttk.Label(self.interior, width=width * 2).grid(column=0, rowspan=2)

    def clear_window(self):
        for wid in self.interior.winfo_children():
            wid.destroy()

    def on_mousewheel(self, event):
        self.canvas.yview_scroll(int(-1 * (event.delta / 120)), "units")


class ScrollEventHandler:

    def __init__(self):
        self.listeners = []

    def add_listener(self, listener):
        self.listeners.append(listener)

    def scroll(self, event):
        for wid in self.listeners:
            if str(wid) + '.' in str(event.widget):
                wid.on_mousewheel(event)
