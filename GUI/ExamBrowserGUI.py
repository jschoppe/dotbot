from tkinter import ttk
from GUI.tkshowimg import size_to_height, convert_image_to_tk


class ExamBrowserGUI:

    def __init__(self, window, height):
        window.title("Exam Viewer")
        self.height = height
        self.clear(window)
        self.main = ttk.Frame(window)
        self.main.grid()

    def set_event_handler(self, eventhandler):
        self.event_handler = eventhandler

    def clear(self, window):
        for wid in window.winfo_children():
            wid.destroy()

    def view_key_examset(self, key):
        self.view_key(key)
        self.view_examset()

    def view_key(self, key):
        self.key = ttk.Label(self.main)
        self._show_image(self.key, self._resize_image(key))
        self.key.grid(column=0, row=0)
        ttk.Label(self.main, text="Image: Key").grid(column=0, row=1,
                                                     sticky="W")

    def view_exam(self, image):
        self.key = ttk.Label(self.main)
        self._show_image(self.key, self._resize_image(image))
        self.key.grid(column=0, row=0)

    def view_examset(self):
        self._create_labels()
        self._display_exams()

    def _display_exams(self):
        self.image.grid(column=1, row=0, columnspan=4)
        self.imnum.grid(column=1, row=1, sticky="W")
        ttk.Button(self.main, text="Prev",
                   command=self.event_handler.prev_image).grid(
                column=2, row=1)
        ttk.Button(self.main, text="Next",
                   command=self.event_handler.next_image).grid(
            column=3, row=1)
        self.grade.grid(column=4, row=1)

    def _create_labels(self):
        self.image = ttk.Label(self.main)
        self.imnum = ttk.Label(self.main)
        self.grade = ttk.Label(self.main, width=15)

    def _resize_image(self, image):
        return size_to_height(image.image(), self.height)

    def _show_image(self, wid, image):
        image = convert_image_to_tk(image)
        wid.image = image
        wid.configure(image=image)

    def display_image(self, image):
        im, number, (id, gd) = image
        self._show_image(self.image, self._resize_image(im))
        self.imnum.configure(text="Exam: " + str(number))
        self.grade.configure(text="ID: " + id + "\n" + gd)
