class BrowserPresenter:

    def __init__(self, view, model, parent):
        self.model = model
        self.view = view
        self.parent = parent
        self.view.set_event_handler(self)

    def display_current_image(self):
        image = self.model.current_image()
        self.view.display_image(image)
        self.parent.show_selected_exam_answers(self.model.image_num())
        if self.model.check_change():
            self.parent.create_button()
            self.model.acknowledge_change()

    def display_key(self):
        self.view.view_key(self.model.key_image())

    def display_exam(self, image_num):
        self.view.view_exam(self.model.image(image_num))

    def display_examset(self):
        self.view.view_examset()
        self.display_current_image()

    def display_key_examset(self):
        self.view.view_key_examset(self.model.key_image())
        self.display_current_image()

    def next_image(self):
        self.model.next_image()
        self.display_current_image()

    def prev_image(self):
        self.model.prev_image()
        self.display_current_image()
