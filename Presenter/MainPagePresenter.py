from Presenter.BrowserPresenter import BrowserPresenter


class MainPagePresenter:

    def __init__(self, view, model, childGUI):
        self.model = model
        self.view = view
        self.view.set_event_handler(self)
        self.view.setup()
        self.browser = childGUI
        self.single_exam_displayed = False

    def load_file(self):
        file = self.view.file_dialog()
        if file != "":
            self.model.set_file(file)
            self.view.setup()
            self.display_key()
            self.view.question_number_dialog(self.model.sampleforms(), self.model.samplefilenames())
            self.display_second_instructions()

    def analyze_key(self, starting, ending, formnumber, formname):
        self.model.set_key_answers(starting, ending, formnumber, formname)
        self.view.insert_label_pairs(starting, ending, 15)
        self.show_exam_key()
        self.view.enable_all_menus()
        self.view.close_browse_window()

    def show_exam_key(self):
        self.model.autoscore_exam(0)
        self.view.insert_key_answers(self.model.get_exam_answers()[0])

    def analyze_examset(self):
        self.view.insert_ids_score_buttons(self.model.autoscore_examset())

    def display_analyzing_progress_bar(self):
        self.view.analyze_all(self.view.insert_ids_score_buttons,
                              self.model.autoscore_examset())

    def display_examset(self):
        win = self.view.get_browse_window()
        BrowserPresenter(self.browser(win, self.view.screen_height), self.model,
                         self).display_examset()
        self.single_exam_displayed = False

    def display_exam_analysis(self):
        self.model.item_analysis()
        self.view.display_exam_analysis(self.model.examset_analysis)

    def display_key(self):
        win = self.view.get_browse_window()
        BrowserPresenter(self.browser(win, self.view.screen_height), self.model,
                         self).display_key()
        self.single_exam_displayed = False

    def display_key_examset(self):
        win = self.view.get_browse_window()
        BrowserPresenter(self.browser(win, self.view.screen_height), self.model,
                         self).display_key_examset()
        self.single_exam_displayed = False

    def display_exam(self, number):
        win = self.view.get_browse_window()
        BrowserPresenter(self.browser(win, self.view.screen_height), self.model,
                         self).display_exam(number)
        self.single_exam_displayed = True

    def display_selected_exam(self, number):
        def display():
            self.show_selected_exam_answers(number)

        return display

    def create_button(self):
        self.view.insert_id_score_button(self.model.current_image()[2],
                                         self.model.image_num())

    def show_selected_exam_answers(self, number):
        def button_command():
            self.display_exam(number)

        self.view.show_exam_answer(self.model.get_exam_answers()[number],
                                   button_command)
        if self.single_exam_displayed:
            button_command()

    def handle_sub_window_closes(self, root):
        self.single_exam_displayed = False
        root.destroy()

    def display_second_instructions(self):
        self.view.change_instructions()

    def display_average(self):
        self.view.display_average(self.model.get_average())

    def display_min_max_scores(self):
        self.view.display_min_max(self.model.get_min_max_scores())

    def display_missed_questions(self):
        self.view.display_missed_questions_histogram(
            self.model.get_missed_questions(),
            self.model.get_num_of_questions())

    def save_to_file(self):
        self.model.save_data_to_file()
